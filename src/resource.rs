use std::collections::HashMap;

use errors::*;

#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize, Clone, Copy)]
pub enum Resource {
    Wood,
    Stone,
    SkillPoint,
    Score,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct ResourceStorage {
    resources: HashMap<Resource, u64>
}

impl ResourceStorage {
    pub fn empty() -> ResourceStorage {
        ResourceStorage {
            resources: HashMap::new()
        }
    }

    pub fn new(resources: Vec<(Resource, u64)>) -> ResourceStorage {
        let mut storage = ResourceStorage::empty();
        storage.add_multiple(resources);
        storage
    }

    pub fn add_multiple(&mut self, resources: Vec<(Resource, u64)>) {
        for (resource, amount) in resources {
            self.add(resource, amount);
        }
    }

    pub fn add(&mut self, resource: Resource, amount: u64) {
        if self.resources.contains_key(&resource) {
            *self.resources.get_mut(&resource).unwrap() += amount;
        } else {
            self.resources.insert(resource, amount);
        }
    }

    pub fn combine(&mut self, other: ResourceStorage) {
        for (res, val) in other.resources {
            self.add(res, val);
        }
    }

    pub fn consume_multiple(&mut self, resources: &Vec<(Resource, u64)>) -> Result<()> {
        let has_enough = resources.iter()
            .all(|&(ref res, amount)| self.get(res) >= amount);

        if has_enough {
            for &(resource, amount) in resources {
                self.consume(resource, amount).unwrap();
            }
            Ok(())
        } else {
            Err("Not enough resources.".into())
        }
    }

    pub fn consume_other(&mut self, other: &ResourceStorage) -> Result<()> {
        self.consume_multiple(&other.get_all())
    }

    pub fn consume_all(&mut self) -> Vec<(Resource, u64)> {
        let result = self.resources.iter_mut()
            .map(|(res, val)| (*res, *val))
            .collect();

        //Set all to 9
        self.resources.iter_mut().for_each(|(_, val)| *val = 0);

        result
    }

    pub fn consume(&mut self, resource: Resource, amount: u64) -> Result<()> {
        match self.resources.get_mut(&resource) {
            //TODO: This is horribly ugly, can somebody make it.. less so?
            Some(ref mut value) if **value >= amount => {
                **value -= amount;
                Ok(())
            }
            _ => Err("Not enough resources.".into())
        }
    }

    pub fn get(&self, resource: &Resource) -> u64 {
        if let Some(&value) = self.resources.get(resource) {
            value
        } else {
            0
        }
    }

    pub fn get_all(&self) -> Vec<(Resource, u64)> {
        let mut list = vec![];
        for (resource, value) in self.resources.iter() {
            list.push((resource.clone(), value.clone()));
        }
        list
    }

    pub fn get_hashmap(&self) -> &HashMap<Resource, u64> {
        &self.resources
    }
}