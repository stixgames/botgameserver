use rand::{thread_rng, Rng};

use map::*;
use entity::*;
use command::*;
use skill::*;

pub trait MobAi {
    fn get_action(&self, map: &Map, entity: &Entity) -> Command;
}

#[derive(Debug, Clone)]
pub struct PeacefulAi {
    pub idle_probability: f32,
}

impl MobAi for PeacefulAi {
    fn get_action(&self, _: &Map, entity: &Entity) -> Command {
        assert_eq!(&entity.get_skills()[0].effect, &Effect::Movement { distance: 1 });
        let mut rng = thread_rng();
        if rng.gen_range(0.0, 1.0) < self.idle_probability {
            Command::Idle
        } else {
            let dir = rng.choose(&[Direction::Up, Direction::Down, Direction::Left, Direction::Right]).unwrap();
            Command::direction_skill(0, *dir)
        }
    }
}

