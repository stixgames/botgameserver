use cgmath::*;

use std::mem;
use std::collections::HashMap;

use math::*;
use map::*;
use entity::*;
use command::*;
use errors::*;
use skill::*;

#[macro_export]
macro_rules! client_error {
    ($entity:ident, $value:expr) => (
        if let Err::<(), Error>(e) = $value {
            let error = e.iter().map(|x| x.to_string()).collect::<Vec<String>>().join(" -> ");
            if $entity.action_error == "" {
                $entity.action_error = error;
            } else {
                $entity.action_error = format!("{} + {}", $entity.action_error, error);
            }
        }
    );
}

pub fn prepare_iteration(entities: Vec<&mut Entity>) {
    for entity in entities {
        //Reset errors
        entity.action_error = "".to_owned();

        entity.last_action = None;

        //Decrease cooldowns
        entity.decrease_cooldowns();
    }
}

pub fn apply_command(entity: &mut Entity, command: Command) -> Result<()> {
    match command {
        Command::Idle => Ok(()),
        Command::Skill { skill_index, direction, target } => {
            let mut chargeup;
            {
                let skill: &mut Skill = entity.get_skill_mut(skill_index)
                    .chain_err(|| ErrorKind::CommandError(format!("Skill index could not be found: {}", skill_index)))?;

                if skill.is_on_cooldown() {
                    bail!(ErrorKind::CommandError(format!("Skill is on cooldown: {}", skill_index)));
                }

                skill.put_on_cooldown()?;
                chargeup = skill.chargeup;
            }

            entity.action = Some(Action {
                skill_index,
                direction,
                target,
                chargeup_left: chargeup,
                full_chargeup: chargeup,
            });

            Ok(())
        }
    }
}

pub fn iterate_game(mut map: &mut Map, entities: Vec<&mut Entity>) {
    let mut blocked_tiles: Vec<Vector2<usize>> = vec![];
    let mut entity_movements: Vec<(&mut Entity, Vec<Vector2<usize>>)> = vec![];
    let mut attacks: Vec<(Vector2<usize>, u16)> = vec![];

    //iterate through all entities
    for entity in entities {
        //Handle all entity actions
        let mut entity_movement = vec![];
        client_error!(entity, handle_entity(entity, &mut map, &mut blocked_tiles, &mut entity_movement, &mut attacks));

        //Insert the original position at index 0
        entity_movement.insert(0, entity.pos);

        entity_movements.push((entity, entity_movement));
    }

    //Collect map data, so blocked fields and damage can be easily checked
    let map_data = collect_map_data(&map, blocked_tiles, attacks);

    //Block movements
    block_movements(&map_data, &mut entity_movements);

    //Resolve movements
    let iteration = resolve_movements(&mut entity_movements);

    //All movement conflicts have been resolved
    for (entity, movement) in entity_movements {
        //Move entity to final position
        entity.pos = get_target_pos(&movement, iteration);

        //Damage entity by damage map
        let pos_index = pos_to_index(entity.pos);
        entity.apply_damage(map_data[pos_index].damage);

        //If the entity is dead, drop loot
        if entity.get_health() <= 0 {
            let drops = entity.drop_kill_reward();
            map.drop_resources(entity.pos, drops);
        }
    }
}

///Prevents entities from moving through blocked areas
fn block_movements(blocked_tiles: &Vec<GameTileData>, entity_movements: &mut Vec<(&mut Entity, Vec<Vector2<usize>>)>) {
    for &mut (_, ref mut movements) in entity_movements {
        let blocked_index = movements
            .iter().enumerate()
            .skip(1)
            .filter(|&(_, pos)| blocked_tiles[pos_to_index(*pos)].blocked)
            .next()
            .and_then(|(i, _)| Some(i));
        if let Some(index) = blocked_index {
            movements.drain(index..);
        }
    }
}

fn resolve_movements(entity_movements: &mut Vec<(&mut Entity, Vec<Vector2<usize>>)>) -> usize {
    let mut iteration = 1;
    while entity_movements.iter().any(|&(_, ref mov)| iteration < mov.len()) {
        //Collect all target positions
        let mut overlapping: HashMap<Vector2<usize>, Vec<usize>> = HashMap::new();
        for (entity_index, &mut (_, ref movement)) in entity_movements.iter_mut().enumerate() {
            let pos = get_target_pos(movement, iteration);
            if overlapping.contains_key(&pos) {
                overlapping.get_mut(&pos).unwrap().push(entity_index);
            } else {
                overlapping.insert(pos, vec![entity_index]);
            }
        }

        //Resolve all position conflicts
        let mut had_conflicts = false;
        for (_, values) in overlapping.iter().filter(|&(_, ref values)| values.len() > 1) {
            had_conflicts = true;

            //Iterate all entities that have the same target position
            for &i in values {
                //Auto attack all other entities with the same target tile
                let damage = entity_movements[i].0.get_auto_attack_damage();
                for &j in values {
                    if i == j {
                        continue;
                    }
                    entity_movements[j].0.apply_damage(damage);
                }

                //Remove all intended movements, including the current one
                entity_movements[i].1.drain(iteration..);
            }
        }

        //If there were conflicts in the current iteration, repeat,
        //in order to prevent conflicts resulting from the current change.
        if !had_conflicts {
            iteration += 1;
        }
    }

    iteration
}

/// Returns the position at index i, or the last one in the vector
fn get_target_pos(movements: &Vec<Vector2<usize>>, i: usize) -> Vector2<usize> {
    assert![movements.len() > 0];

    if i >= movements.len() {
        *movements.last().unwrap()
    } else {
        movements[i]
    }
}

struct GameTileData {
    pub blocked: bool,
    pub damage: u16,
}

impl GameTileData {
    pub fn new(blocked: bool, damage: u16) -> GameTileData {
        GameTileData {
            blocked,
            damage,
        }
    }
}

fn collect_map_data(map: &Map, blocked_tiles: Vec<Vector2<usize>>, attacks: Vec<(Vector2<usize>, u16)>) -> Vec<GameTileData> {
    let mut map_data: Vec<GameTileData> =
        map.get_tile_data()
            .iter()
            .map(|tile_type| GameTileData::new(tile_type.is_blocking, 0))
            .collect();

    for blocked in blocked_tiles {
        map_data[pos_to_index(blocked)].blocked = true;
    }

    for attack in attacks {
        map_data[pos_to_index(attack.0)].damage += attack.1;
    }

    map_data
}

/// Handles the action of the current entity, including applying passives and returning intended movements and attacks.
fn handle_entity(entity: &mut Entity, map: &mut  Map, blocked_tiles: &mut Vec<Vector2<usize>>, movements: &mut Vec<Vector2<usize>>, attacks: &mut Vec<(Vector2<usize>, u16)>) -> Result<()> {
    let is_charged = entity.action.as_mut()
        .ok_or("no action".to_owned())
        .map(|action| {
            if action.chargeup_left > 0 {
                action.chargeup_left -= 1;
                false
            } else {
                true
            }
        })?;

    if is_charged {
        //Consume the current action, replace it with an empty one.
        let action = mem::replace(&mut entity.action, None).unwrap();

        //Uses the skill, then clones the skill, in case the entity is changed
        let skill = entity.use_skill(action.skill_index)?.clone();

        //Remove all skills that have been used up
        entity.remove_used_skills();

        //Blocked tiles
        let block_result = skill.effect
            .get_block(&action)
            .and_then(|x| Ok(x
                .iter()
                .map(|coord| local_to_global(entity, coord))
                .for_each(|coord| blocked_tiles.push(coord))));

        //Movements
        let move_result = skill.effect
            .get_movement(&action)
            .and_then(|x| Ok(x
                .iter()
                .map(|coord| local_to_global(entity, coord))
                .for_each(|coord| movements.push(coord))));

        //Attacks
        let attack_result = skill.effect
            .get_attack(&action)
            .and_then(|x| Ok(x
                .iter()
                .for_each(|attack| {
                    attacks.push((local_to_global(entity, &attack.0), attack.1))
                })));

        //Passives
        let passive_result = skill.effect.apply_other(entity, map, &action);

        entity.last_action = Some(action);

        //TODO: when there are multiple results, this only returns the first. Maybe create a new ErrorKind for parallel errors?
        block_result.and(move_result).and(attack_result).and(passive_result)
    } else {
        Ok(())
    }
}

fn local_to_global(entity: &Entity, local: &Vector2<i16>) -> Vector2<usize> {
    vec2_i16_usize(pos_mod(vec2_usize_i16(entity.pos) + local, MAP_SIZE))
}

#[cfg(test)]
mod tests {
    use cgmath::*;

    use item::*;
    use game::*;

    fn setup_player(pos: Vector2<usize>) -> Entity {
        let weapon = Item::new_simple_weapon("Sword", 10);
        let movement = Skill::new_continuous("Move", Effect::Movement { distance: 1 });
        let charge = Skill::new("Charge", Effect::Movement { distance: 5 }, None, 1, 0);
        Entity::new("TestPlayer".to_owned(), pos, 100, vec![weapon], vec![movement, charge])
    }

    #[test]
    fn game_movement_skill_execution_test() {
        use game::iterate_game;

        let mut map = Map::empty();

        let mut players_original: Vec<Entity> = vec![
            setup_player(vec2(0, 0)),
            setup_player(vec2(2, 0)),
            setup_player(vec2(5, 0)),
        ];

        let mut players: Vec<&mut Entity> = players_original.iter_mut().collect();

        players[0].action = Some(Action::direction(0, Direction::Down, 0));
        players[1].action = Some(Action::direction(0, Direction::Down, 0));
        players[2].action = Some(Action::direction(0, Direction::Up, 0));
        iterate_game(&mut map, &mut players);
        assert_eq!(vec2(0, 1), players[0].pos);
        assert_eq!(vec2(2, 1), players[1].pos);
        assert_eq!(vec2(5, 99), players[2].pos);

        players[0].action = Some(Action::direction(0, Direction::Right, 0));
        players[1].action = Some(Action::direction(0, Direction::Left, 0));
        players[2].action = Some(Action::direction(0, Direction::Up, 0));
        iterate_game(&mut map, &mut players);
        assert_eq!(vec2(0, 1), players[0].pos);
        assert_eq!(vec2(2, 1), players[1].pos);
        assert_eq!(vec2(5, 98), players[2].pos);
        assert_eq!(90, players[0].get_health());
        assert_eq!(90, players[1].get_health());

        players[0].action = Some(Action::direction(0, Direction::Up, 0));
        players[1].action = Some(Action::direction(0, Direction::Down, 0));
        iterate_game(&mut map, &mut players);
        assert_eq!(vec2(0, 0), players[0].pos);
        assert_eq!(vec2(2, 2), players[1].pos);
        assert_eq!(vec2(5, 98), players[2].pos);

        players[0].action = Some(Action::direction(1, Direction::Down, 1));
        players[1].action = Some(Action::direction(1, Direction::Left, 1));
        iterate_game(&mut map, &mut players);
        assert_eq!(vec2(0, 0), players[0].pos);
        assert_eq!(vec2(2, 2), players[1].pos);
        iterate_game(&mut map, &mut players);
        assert_eq!(vec2(0, 1), players[0].pos);
        assert_eq!(vec2(1, 2), players[1].pos);
        assert_eq!(80, players[0].get_health());
        assert_eq!(80, players[1].get_health());

        players[1].action = Some(Action::direction(0, Direction::Right, 0));
        iterate_game(&mut map, &mut players);
        assert_eq!(vec2(0, 1), players[0].pos);
        assert_eq!(vec2(2, 2), players[1].pos);

        players[0].action = Some(Action::direction(1, Direction::Right, 1));
        players[1].action = Some(Action::direction(1, Direction::Up, 1));
        iterate_game(&mut map, &mut players);
        assert_eq!(vec2(0, 1), players[0].pos);
        assert_eq!(vec2(2, 2), players[1].pos);
        iterate_game(&mut map, &mut players);
        assert_eq!(vec2(5, 1), players[0].pos);
        assert_eq!(vec2(2, 97), players[1].pos);
        assert_eq!(80, players[0].get_health());
        assert_eq!(80, players[1].get_health());
    }
}