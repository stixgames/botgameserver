use cgmath::*;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub struct Action {
    pub skill_index: usize,
    pub direction: Option<Direction>,
    pub target: Option<Vector2<i16>>,
    pub chargeup_left: u8,
    pub full_chargeup: u8,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Command {
    Idle,
    Skill {
        skill_index: usize,
        direction: Option<Direction>,
        target: Option<Vector2<i16>>,
    },
}

impl Command {
    pub fn direction_skill(skill_index: usize, dir: Direction) -> Command {
        Command::Skill {
            skill_index,
            direction: Some(dir),
            target: None
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash)]
pub enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl Direction {
    ///Returns a vector, representing the direction
    ///
    /// # Examples
    /// ´´´
    /// let
    /// ´´´
    pub fn get_direction(&self) -> Vector2<i16> {
        match *self {
            Direction::Left => vec2(-1, 0),
            Direction::Right => vec2(1, 0),
            Direction::Up => vec2(0, -1),
            Direction::Down => vec2(0, 1),
        }
    }
}

#[cfg(test)]
mod tests {
    use cgmath::*;

    use skill::*;
    use command::*;

    #[test]
    fn skill_effect_test() {
        let effect = Effect::Movement { distance: 1 };
        let action = Action::direction(0, Direction::Down, 0);
        assert_eq!(vec![vec2(0, 1)], effect.get_movement(&action).unwrap());
        let action = Action::direction(0, Direction::Up, 0);
        assert_eq!(vec![vec2(0, -1)], effect.get_movement(&action).unwrap());
        let action = Action::direction(0, Direction::Left, 0);
        assert_eq!(vec![vec2(-1, 0)], effect.get_movement(&action).unwrap());
        let action = Action::direction(0, Direction::Right, 0);
        assert_eq!(vec![vec2(1, 0)], effect.get_movement(&action).unwrap());
    }

    #[test]
    fn skill_effect_test_2() {
        let effect = Effect::Movement { distance: 4 };
        let action = Action::direction(0, Direction::Down, 0);
        assert_eq!(vec![vec2(0, 1), vec2(0, 2), vec2(0, 3), vec2(0, 4)], effect.get_movement(&action).unwrap());
        let action = Action::direction(0, Direction::Up, 0);
        assert_eq!(vec![vec2(0, -1), vec2(0, -2), vec2(0, -3), vec2(0, -4)], effect.get_movement(&action).unwrap());
        let action = Action::direction(0, Direction::Left, 0);
        assert_eq!(vec![vec2(-1, 0), vec2(-2, 0), vec2(-3, 0), vec2(-4, 0)], effect.get_movement(&action).unwrap());
        let action = Action::direction(0, Direction::Right, 0);
        assert_eq!(vec![vec2(1, 0), vec2(2, 0), vec2(3, 0), vec2(4, 0)], effect.get_movement(&action).unwrap());
    }
}