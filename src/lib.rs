#![feature(deadline_api)]

#[macro_use]
extern crate error_chain;

#[macro_use]
extern crate serde_derive;
pub extern crate serde;
pub extern crate serde_json;
extern crate flate2;

pub extern crate cgmath;

extern crate byteorder;

pub extern crate rand;

#[macro_use]
pub mod game;
pub mod map;
pub mod entity;
pub mod item;
pub mod skill;
pub mod command;
pub mod math;
pub mod server;
pub mod errors;
pub mod mob_ai;
pub mod resource;
