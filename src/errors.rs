use std;
use serde_json;

error_chain! {
    errors {
        ConnectionFailed(reason: String) {
            description("Connection failure")
            display("Connection failure: '{}'", reason)
        }
        CommandError(reason: String) {
            description("Command was not valid")
            display("Command was not valid: '{}'", reason)
        }
        RequestTimeout {
            description("You took too long to respond")
            display("You took too long to respond.")
        }
    }

    foreign_links {
        SerdeJson(serde_json::Error);
        Io(std::io::Error);
        MpscRecv(std::sync::mpsc::RecvError);
    }
}