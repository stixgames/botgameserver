use std;
use cgmath::Vector2;

use item::*;
use skill::*;
use command::*;
use errors::*;
use resource::*;

const MAX_CONSUMABLES: usize = 5;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Entity {
    pub name: String,
    pub entity_symbol: String,
    pub pos: Vector2<usize>,
    health: u16,
    pub resources: ResourceStorage,
    kill_reward: ResourceStorage,
    items: Vec<Item>,
    pub skills: Vec<Skill>,
    pub action: Option<Action>,
    pub last_action: Option<Action>,
    pub action_error: String,
}

impl Entity {
    pub fn new(name: String, entity_symbol: String, pos: Vector2<usize>, health: u16, kill_reward: Vec<(Resource, u64)>, items: Vec<Item>, skills: Vec<Skill>) -> Entity {
        Entity {
            name,
            entity_symbol,
            pos,
            health,
            resources: ResourceStorage::empty(),
            kill_reward: ResourceStorage::new(kill_reward),
            items,
            skills,
            action: None,
            last_action: None,
            action_error: "".to_owned(),
        }
    }

    pub fn get_skills(&self) -> Vec<&Skill> {
        self.skills.iter()
            .chain(self.items.iter().filter_map(|x| x.get_item_skill()))
            .collect()
    }

    pub fn get_skills_mut(&mut self) -> Vec<&mut Skill> {
        self.skills.iter_mut()
            .chain(self.items.iter_mut().filter_map(|x| x.get_item_skill_mut()))
            .collect()
    }

    pub fn get_skill(&self, i: usize) -> Result<&Skill> {
        if i >= self.get_skills().len() {
            bail!("Skill index out of range".to_owned());
        }

        Ok(self.get_skills().remove(i))
    }

    pub fn get_skill_mut(&mut self, i: usize) -> Result<&mut Skill> {
        if i >= self.get_skills().len() {
            bail!("Skill index out of range".to_owned());
        }

        Ok(self.get_skills_mut().remove(i))
    }

    pub fn decrease_cooldowns(&mut self) {
        self.get_skills_mut().iter_mut().for_each(|skill| skill.decrease_cooldown());
    }

    pub fn use_skill(&mut self, i: usize) -> Result<&Skill> {
        let costs = self.get_skill(i).chain_err(|| "Could not find skill")?.costs.clone();
        self.resources.consume_other(&costs).chain_err(|| "Could not use skill")?;

        let skill = self.get_skill_mut(i).chain_err(|| "Could not find skill")?;

        skill.decrease_uses();
        Ok(skill)
    }

    pub fn remove_used_skills(&mut self) {
        self.skills.retain(|skill| skill.uses != Some(0));
        self.items.retain(|item|
            match item.get_item_skill() {
                Some(skill) => skill.uses != Some(0),
                None => true,
            });
    }

    pub fn get_auto_attack_damage(&self) -> u16 {
        self.items.iter()
            .map(|item| {
                match item.item_data {
                    ItemData::Weapon { attack } => attack,
                    _ => 0,
                }
            })
            .sum()
    }

    pub fn get_health(&self) -> u16 {
        self.health
    }

    pub fn apply_damage(&mut self, damage: u16) {
        if self.health > damage {
            self.health -= damage;
        } else {
            self.health = 0;
        }
    }

    pub fn heal(&mut self, heal: u16) {
        if self.is_dead() {
            //Can't heal what's already dead.
        } else {
            self.health += heal;
        }
    }

    pub fn is_dead(&self) -> bool {
        self.health == 0
    }

    pub fn get_action(&self) -> &Option<Action> {
        &self.action
    }

    fn max_item_type(&self, item: &Item) -> usize {
        match *item.get_item_data() {
            ItemData::Weapon { attack: _ } => 1,
            ItemData::Armor { defense: _ } => 1,
            ItemData::Consumable { skill: _ } => MAX_CONSUMABLES,
        }
    }

    //Adds a item, returns either the item itself when there is no space, or the item it replaced
    pub fn add_item(&mut self, item: Item) -> Option<Item> {
        let max_item_count = self.max_item_type(&item);
        let mut has_space = false;
        let mut replace_index = None;
        match item.get_item_data() {
            //TODO: Refactor this thing
            &ItemData::Weapon { attack } => {
                let weapon_attacks = self.items.iter()
                    .enumerate()
                    .filter_map(|(i, item)| if let ItemData::Weapon { attack } = item.item_data {
                        Some((i, attack))
                    } else { None })
                    .collect::<Vec<(usize, u16)>>();
                let weapon_count = weapon_attacks.len();
                if weapon_count < max_item_count {
                    has_space = true;
                } else {
                    let weakest_weapon = weapon_attacks.iter().min_by_key(|&&(_, attack)| attack);
                    if let Some(&(i, a)) = weakest_weapon {
                        if attack > a {
                            replace_index = Some(i);
                        }
                    }
                }
            }
            &ItemData::Armor { defense } => {
                let armor_defenses = self.items.iter()
                    .enumerate()
                    .filter_map(|(i, item)| if let ItemData::Armor { defense } = item.item_data {
                        Some((i, defense))
                    } else { None })
                    .collect::<Vec<(usize, u16)>>();;
                let armor_count = armor_defenses.len();
                if armor_count < max_item_count {
                    has_space = true;
                } else {
                    let weakest_armor = armor_defenses.iter().min_by_key(|&&(_, defense)| defense);
                    if let Some(&(i, a)) = weakest_armor {
                        if defense > a {
                            replace_index = Some(i);
                        }
                    }
                }
            }
            &ItemData::Consumable { skill: _ } => {
                let consumable_count = self.items.iter().filter(|x| x.is_consumable()).count();

                if consumable_count < max_item_count {
                    has_space = true;
                }

                //Consumables can't be replaced
            }
        }

        if has_space {
            //Add the item to the list
            self.items.push(item);
            None
        } else if let Some(index) = replace_index {
            //Replace the weaker item with the new one
            Some(std::mem::replace(self.items.get_mut(index)?, item))
        } else {
            //There's no space left, send the new item back
            Some(item)
        }
    }

    pub fn drop_kill_reward(&self) -> ResourceStorage {
        self.kill_reward.clone()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EntityData {
    pub name: String,
    pub health: u16,
    pub items: Vec<Item>,
    pub skills: Vec<Skill>,
    pub action: Option<Action>,
    pub action_error: String,
}