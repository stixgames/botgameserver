use cgmath::*;

pub fn magnitude(v: Vector2<i16>) -> f32 {
    vec2(v.x as f32, v.y as f32).magnitude()
}

pub fn pos_mod(pos: Vector2<i16>, m: usize) -> Vector2<i16> {
    vec2(pos.x.modulo(m as i16), pos.y.modulo(m as i16))
}

pub fn vec2_usize_i16(item: Vector2<usize>) -> Vector2<i16> {
    vec2(item.x as i16, item.y as i16)
}

pub fn vec2_i16_usize(item: Vector2<i16>) -> Vector2<usize> {
    vec2(item.x as usize, item.y as usize)
}

///
/// Modulo that handles negative numbers, works the same as Python's `%`.
///
/// eg: `(a + b).modulo(c)`
///
pub trait ModuloSignedExt {
    fn modulo(&self, n: Self) -> Self;
}
macro_rules! modulo_signed_ext_impl {
    ($($t:ty)*) => ($(
        impl ModuloSignedExt for $t {
            #[inline]
            fn modulo(&self, n: Self) -> Self {
                (self % n + n) % n
            }
        }
    )*)
}
modulo_signed_ext_impl! { i8 i16 i32 i64 }