use std::sync::mpsc::{Sender, Receiver, RecvError, TryRecvError, RecvTimeoutError};
use std::sync::mpsc;
use std::thread;
use std::collections::HashMap;
use std;
use rand::{thread_rng, Rng};
use std::io::prelude::*;
use std::net::{TcpListener, TcpStream};
use serde;
use serde_json;
use std::time::{Instant, Duration};
use error_chain::ChainedError;
use flate2::write::GzEncoder;
use flate2::read::GzDecoder;
use flate2::Compression;
use std::io;

use game::*;
use entity::*;
use map::*;
use item::*;
use skill::*;
use errors::*;
use command::*;
use mob_ai::*;
use resource::*;

pub struct Server {
    map: Map,
    start_items: Vec<Item>,
    start_skills: Vec<Skill>,
    clients: HashMap<String, Client>,
    mobs: Vec<(Box<MobAi>, Entity)>,
    dead_players: Vec<Entity>,
    rcons: Vec<BiChannel>,
    admin_pw: String,
    request_timeout: Duration,
}

struct Client {
    pub entity: Entity,
    pub inout: BiChannel,
    pub is_connected: bool,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Request {
    Connection { name: String },
    RconConnect { password: String },
    Reconnect { key: String },
    ExecuteCommand(Command),
    RconRestart,
    RconChangeTimeout(Duration),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Response {
    RconAccepted,
    PlayerAccepted { key: String },
    GameState(GameState),
    Error(String),
    ConnectionRefused(String),
}

struct BiChannel {
    tx: Sender<Response>,
    rx: Receiver<Request>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GameState {
    surrounding_area: Vec<GameTile>,
    player_pos: Vector2<usize>,
    area_size: usize,
    tile_data: Vec<TileType>,
}

macro_rules! connection_failed {
    ($tx:expr,$err:expr) => {{
        $tx.send(Response::Error($err)).chain_err(|| "Sending error message to client failed.")?;
        bail!(ErrorKind::ConnectionFailed($err));
    }}
}

impl Server {
    pub fn new(map: Map, admin_password: String, items: Vec<Item>, skills: Vec<Skill>) -> Server {
        let mobs = generate_mobs(&map);

        Server {
            map: map,
            start_items: items,
            start_skills: skills,
            clients: HashMap::new(),
            rcons: vec![],
            mobs: mobs,
            dead_players: vec![],
            admin_pw: admin_password,
            request_timeout: Duration::from_secs(20),
        }
    }

    pub fn start(&mut self, ip_port: &'static str) -> Result<()> {
        println!("Start Server");
        let (bi_tx, bi_rx) = mpsc::channel();

        thread::spawn(move || {
            let listener = TcpListener::bind(ip_port).unwrap();
            for mut connection in listener.incoming() {
                if let Err(e) = connection.chain_err(|| "IO Error")
                    .and_then(|stream| {
                        input_loop(stream, bi_tx.clone())
                    })
                    {
                        println!("Connection error: {}", e.display_chain());
                    }
            }
        });

        loop {
            //Add new players
            loop {
                match bi_rx.try_recv() {
                    Ok(input) => {
                        let result = self.handle_new_client(input);
                        if let Err(e) = result {
                            println!("Error while connecting to client: {}", e.display_chain());
                        }
                    }
                    Err(TryRecvError::Empty) => break,
                    Err(TryRecvError::Disconnected) => bail!("Server thread disconnected."),
                };
            }

            //Collect all entities
            //Only send actions, when the action is on chargeup,
            //to prevent new players from getting already locked in actions from other players
            let mut all_entities: Vec<Entity> = self.get_all_entities().iter().map(|&e| e.clone()).collect();
            for entity in all_entities.iter_mut() {
                if entity.action.iter().any(|ref action| action.chargeup_left == action.full_chargeup) {
                    entity.action = None;
                }
            }

            //Send current state to each player
            for (_, client) in self.clients.iter_mut().filter(|&(_, ref client)| client.is_connected) {
                let (game_tiles, player_pos) = self.map.get_surrounding(&all_entities, client.entity.pos);

                //Create the gamestate object
                let data = GameState {
                    surrounding_area: game_tiles,
                    player_pos,
                    area_size: VIEW_DIAMETER,
                    tile_data: self.map.tile_types.clone(),
                };

                if let Err(e) = client.inout.tx.send(Response::GameState(data)) {
                    println!("Error while sending data to client: {}", e);
                }
            }

            //Prepare entities for next turn
            prepare_iteration(self.get_all_entities_mut());

            //Read commands
            let deadline = Instant::now() + self.request_timeout;
            for client in self.clients.iter_mut().map(|(_, client)| client) {
                let entity = &mut client.entity;
                //match client.inout.rx.recv_deadline(deadline) {
                match client.inout.rx.recv() {
                    Ok(Request::ExecuteCommand(command)) => client_error!(entity, apply_command(entity, command)),
                    Ok(request) => client_error!(entity, Err(ErrorKind::Msg(format!("Invalid request in the current state: {:?}", request)).into())),
                    //Err(RecvTimeoutError::Timeout) => client_error!(entity, Err(ErrorKind::RequestTimeout.into())),
                    //Err(RecvTimeoutError::Disconnected) => client.is_connected = false,
                    Err(RecvError) => client.is_connected = false,
                }
            }

            //Get commands for all mobs
            for &mut (ref ai, ref mut mob) in self.mobs.iter_mut() {
                let command = ai.get_action(&self.map, mob);
                client_error!(mob, apply_command(mob, command));
            }

            //Iterate game
            iterate_game(&mut self.map, get_all_entities_mut_non_blocking(&mut self.clients, &mut self.mobs));

            //Collect all dead players
            let mut dead_player_keys = vec![];
            for (key, client) in self.clients.iter()
                .filter(|&(_, ref client)| client.entity.get_health() == 0) {
                self.dead_players.push(client.entity.clone());
                dead_player_keys.push(key.clone());
            }
            for key in dead_player_keys {
                self.clients.remove(&key);
            }

            //Remove all dead mobs
            self.mobs.retain(|&(_, ref mob)| mob.get_health() > 0);
        }
    }

    fn get_all_entities(&self) -> Vec<&Entity> {
        get_all_entities_non_blocking(&self.clients, &self.mobs)
    }

    fn get_all_entities_mut(&mut self) -> Vec<&mut Entity> {
        get_all_entities_mut_non_blocking(&mut self.clients, &mut self.mobs)
    }

    fn handle_new_client(&mut self, inout: BiChannel) -> Result<()> {
        inout.rx.recv()
            .chain_err(|| "Receive Error")
            .and_then(|request| {
                match request {
                    Request::Connection { name } => {
                        let output_name = name.clone();
                        if self.clients.iter().any(|(_, client)| client.entity.name == name) {
                            connection_failed!(inout.tx, "There is already a player with this name.".to_owned())
                        }

                        let connection_key = self.create_unique_key();
                        inout.tx.send(Response::PlayerAccepted { key: connection_key.clone() })
                            .chain_err(|| "Sending response to client failed")?;
                        let new_player = self.create_player(name);
                        self.clients.insert(connection_key, Client {
                            entity: new_player,
                            inout,
                            is_connected: true,
                        });
                        println!("New player connected: {}", output_name);
                        Ok(())
                    }
                    Request::Reconnect { key } => {
                        match self.clients.get_mut(&key) {
                            Some(mut client) => {
                                inout.tx.send(Response::PlayerAccepted { key })
                                    .chain_err(|| "Sending response to client failed")?;
                                client.inout = inout;
                                client.is_connected = true;
                            }
                            None => connection_failed!(inout.tx, "Key not found.".to_owned())
                        }
                        Ok(())
                    }
                    Request::RconConnect { password } => {
                        if password != self.admin_pw {
                            connection_failed!(inout.tx, "Wrong password!".to_owned())
                        }
                        inout.tx.send(Response::RconAccepted)
                            .chain_err(|| "Sending response to client failed")?;
                        self.rcons.push(inout);
                        Ok(())
                    }
                    _ => connection_failed!(inout.tx, "Request not accepted while connecting.".to_owned()),
                }
            }).chain_err(|| "Client connection error")
    }

    fn create_player(&self, name: String) -> Entity {
        let pos = get_empty_pos(&self.map, self.get_all_entities());

        Entity::new(name, "☺".to_owned(), pos, 100, vec![(Resource::Score, 1000)],self.start_items.clone(), self.start_skills.clone())
    }

    fn create_unique_key(&self) -> String {
        let mut key;
        loop {
            key = thread_rng().gen_ascii_chars().take(10).collect::<String>();

            if !self.clients.contains_key(&key) {
                break;
            }
        }
        key
    }
}

fn get_all_entities_non_blocking<'a>(clients: &'a HashMap<String, Client>, mobs: &'a Vec<(Box<MobAi>, Entity)>) -> Vec<&'a Entity> {
    let players = clients.iter().map(|(_, ref client)| &client.entity);
    let mobs = mobs.iter().map(|&(_, ref mob)| mob);
    players.chain(mobs).collect()
}

fn get_all_entities_mut_non_blocking<'a>(clients: &'a mut HashMap<String, Client>, mobs: &'a mut Vec<(Box<MobAi>, Entity)>) -> Vec<&'a mut Entity> {
    let players = clients.iter_mut().map(|(_, client)| &mut client.entity);
    let mobs = mobs.iter_mut().map(|&mut (_, ref mut mob)| mob);
    players.chain(mobs).collect()
}

fn generate_mobs(map: &Map) -> Vec<(Box<MobAi>, Entity)> {
    let mut mobs: Vec<(Box<MobAi>, Entity)> = vec![];

    let sheep_skills = vec![
        Skill::new_continuous("Move", Effect::Movement { distance: 1 }),
    ];

    let sheep_ai = PeacefulAi { idle_probability: 0.5 };

    for _ in 0..300 {
        let pos = get_empty_pos(&map, mobs.iter().map(|&(_, ref entity)| entity).collect());

        let sheep = Entity::new("Sheep".to_owned(), "s".to_owned(), pos, 10, vec![(Resource::Score, 10), (Resource::SkillPoint, 1)], vec![], sheep_skills.clone());
        mobs.push((Box::new(sheep_ai.clone()), sheep));
    }

    mobs
}

fn get_empty_pos(map: &Map, entities: Vec<&Entity>) -> Vector2<usize> {
    //Generate random positions, until a empty one is found
    let mut rng = thread_rng();
    let mut pos;
    loop {
        pos = vec2(rng.gen_range(0, MAP_SIZE), rng.gen_range(0, MAP_SIZE));

        if !map.get_tile_type(pos).is_blocking && entities.iter().all(|e| e.pos != pos) {
            break;
        }
    }
    pos
}

fn input_loop(mut stream: TcpStream, bi_rx: Sender<BiChannel>) -> Result<()> {
    let (request_tx, request_rx) = mpsc::channel();
    let (response_tx, response_rx) = mpsc::channel();

    //Send channels to main thread
    bi_rx.send(BiChannel { tx: response_tx, rx: request_rx })
        .chain_err(|| "Sending channels to main thread failed.")?;

    //The first request is the connection acceptor, after that, the normal stuff follows
    let request: Request = read_json(&mut stream).chain_err(|| "Reading request from client failed")?;
    request_tx.send(request).chain_err(|| "Sending request to main thread failed")?;
    let response = response_rx.recv().chain_err(|| "Receiving response from main thread failed")?;
    write_json(&mut stream, response).chain_err(|| "sending to client failed")?;

    loop {
        let response = response_rx.recv().chain_err(|| "Receiving response from main thread failed")?;
        write_json(&mut stream, response).chain_err(|| "sending to client failed")?;

        let request: Request = read_json(&mut stream).chain_err(|| "Reading request from client failed")?;
        request_tx.send(request).chain_err(|| "Sending request to main thread failed")?;
    }
}

fn write_json<T>(stream: &mut TcpStream, value: T) -> Result<()>
    where T: serde::Serialize
{
    use byteorder::{LittleEndian, WriteBytesExt};

    //Serialize and compress
    let mut encoder = GzEncoder::new(Vec::new(), Compression::default());
    encoder.write(&serde_json::to_vec(&value)?)?;

    let serialized = encoder.finish()?;
    let mut length = vec![];
    length.write_u32::<LittleEndian>(serialized.len() as u32)?;
    println!("Sending {} bytes", serialized.len());

    stream.write(&length)?;
    stream.write(&serialized)?;
    Ok(())
}

fn read_json<T>(stream: &mut TcpStream) -> Result<T>
    where T: serde::de::DeserializeOwned
{
    use std::io::Cursor;
    use byteorder::{LittleEndian, ReadBytesExt};

    let mut length_buf = vec![0; 4];

    stream.read_exact(&mut length_buf)?;
    let length = Cursor::new(length_buf).read_u32::<LittleEndian>()?;
    println!("Recieved {} bytes", length);

    let mut compressed_data = vec![0 as u8; length as usize];
    stream.read_exact(&mut compressed_data)?;

    //Decompress
    let mut d = GzDecoder::new(compressed_data.as_slice());

    let mut data = vec![];
    d.read_to_end(&mut data)?;

    let obj: T = serde_json::from_slice(&data).chain_err(|| format!("Couldn't parse json: '{:?}'", std::str::from_utf8(&data)))?;

    Ok(obj)
}