use std::collections::HashMap;
use rand::{thread_rng, Rng};

use cgmath::*;

use math::*;
use entity::*;
use resource::*;
use item::*;

pub const MAP_SIZE: usize = 100;
pub const MAP_TILES: usize = MAP_SIZE * MAP_SIZE;

const VIEW_RADIUS: usize = 5;
pub const VIEW_DIAMETER: usize = VIEW_RADIUS * 2 + 1;
pub const VIEW_TILES: usize = VIEW_DIAMETER * VIEW_DIAMETER;

#[derive(Debug, Clone, Serialize, Deserialize)]
///The tile type the clients will recieve
pub struct GameTile {
    tile: Tile,
    entity: Option<Entity>,
    resource_drops: HashMap<Resource, u64>,
    item_drops: Vec<Item>,
}

impl GameTile {
    pub fn new(tile: Tile) -> GameTile {
        GameTile {
            tile,
            entity: None,
            resource_drops: HashMap::new(),
            item_drops: vec![],
        }
    }
}

pub struct Map {
    data: [Tile; MAP_TILES],
    resource_drops: HashMap<Vector2<usize>, ResourceStorage>,
    item_drops: HashMap<Vector2<usize>, Vec<Item>>,
    pub tile_types: Vec<TileType>,
}

impl Map {
    pub fn empty() -> Map {
        Map {
            data: [Tile::empty(); MAP_TILES],
            resource_drops: HashMap::new(),
            item_drops: HashMap::new(),
            tile_types: vec![TileType::new("Empty", " ", false, vec![])],
        }
    }

    pub fn create_map() -> Map {
        let mut map = Map::empty();

        map.tile_types.push(TileType::new("Tree", "↑", true, vec![(Resource::Wood, 1)]));
        map.tile_types.push(TileType::new("Rock", "o", true, vec![(Resource::Stone, 1)]));

        let mut rng = thread_rng();
        for tile in map.data.iter_mut() {
            let val = rng.gen_range(0.0, 1.0);
            if val < 0.1 {
                tile.id = 1;
            } else if val < 0.15 {
                tile.id = 2;
            }
        }

        map
    }

    pub fn get_surrounding(&self, entities: &Vec<Entity>, pos: Vector2<usize>) -> (Vec<GameTile>, Vector2<usize>) {
        let start = vec2(pos.x as i16 - VIEW_RADIUS as i16, pos.y as i16 - VIEW_RADIUS as i16);
        let end = vec2(pos.x as i16 + VIEW_RADIUS as i16, pos.y as i16 + VIEW_RADIUS as i16);

        let mut result = vec![];

        let mut i = 0;
        for y in (start.y)..(end.y + 1) {
            for x in (start.x)..(end.x + 1) {
                let pos = vec2_i16_usize(pos_mod(vec2(x, y), MAP_SIZE));

                result.push(GameTile::new(self.get_tile(pos)));

                if let Some(resource) = self.resource_drops.get(&pos) {
                    result[i].resource_drops = resource.get_hashmap().clone();
                }

                if let Some(items) = self.item_drops.get(&pos) {
                    result[i].item_drops = items.clone();
                }
                i += 1;
            }
        }

        for entity in entities {
            let global_pos = entity.pos;
            let local_pos = vec2_usize_i16(global_pos) - start;
            if local_pos.x >= 0 && local_pos.y >= 0 && local_pos.x < VIEW_DIAMETER as i16 && local_pos.y < VIEW_DIAMETER as i16 {
                //Set the position of the entity to local coordinates
                let mut local_entity = entity.clone();
                let p = vec2_i16_usize(local_pos);
                local_entity.pos = p;

                result[local_pos_to_index(p)].entity = Some(local_entity);
            }
        }

        let local_player_pos = vec2_i16_usize(vec2_usize_i16(pos) - start);
        (result, local_player_pos)
    }

    pub fn get_tile(&self, pos: Vector2<usize>) -> Tile {
        self.data[pos_to_index(pos)]
    }

    pub fn get_tile_data(&self) -> Vec<&TileType> {
        self.data.iter().map(|tile| &self.tile_types[tile.id]).collect()
    }

    pub fn get_tile_type(&self, pos: Vector2<usize>) -> &TileType {
        &self.tile_types[self.data[pos_to_index(pos)].id]
    }

    pub fn drop_resources(&mut self, pos: Vector2<usize>, resources: ResourceStorage) {
        if self.resource_drops.contains_key(&pos) {
            self.resource_drops.get_mut(&pos).unwrap().combine(resources);
        } else {
            self.resource_drops.insert(pos, resources);
        }
    }

    pub fn collect_resources(&mut self, pos: Vector2<usize>) -> ResourceStorage {
        if let Some(resources) = self.resource_drops.remove(&pos) {
            resources
        } else {
            ResourceStorage::empty()
        }
    }

    pub fn drop_items(&mut self, pos: Vector2<usize>, mut items: Vec<Item>) {
        if self.item_drops.contains_key(&pos) {
            self.item_drops.get_mut(&pos).unwrap().append(&mut items);
        } else {
            self.item_drops.insert(pos, items);
        }
    }

    pub fn collect_items(&mut self, pos: Vector2<usize>) -> Vec<Item> {
        if let Some(items) = self.item_drops.remove(&pos) {
            items
        } else {
            vec![]
        }
    }
}

pub fn pos_to_index(pos: Vector2<usize>) -> usize {
    pos.x as usize + pos.y as usize * MAP_SIZE
}

pub fn local_pos_to_index(pos: Vector2<usize>) -> usize {
    pos.x as usize + pos.y as usize * VIEW_DIAMETER
}

pub fn offset_pos(pos: Vector2<usize>, offset: Vector2<i16>) -> Vector2<usize> {
    vec2_i16_usize(pos_mod(vec2_usize_i16(pos) + offset, MAP_SIZE))
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq)]
pub struct Tile {
    pub id: usize,
}

impl Tile {
    pub fn empty() -> Tile {
        Tile {
            id: 0,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TileType {
    pub name: String,
    pub map_symbol: String,
    pub is_blocking: bool,
    pub resources_per_harvest: ResourceStorage,
}

impl TileType {
    pub fn new(name: &str, map_symbol: &str, is_blocking: bool, resources_per_harvest: Vec<(Resource, u64)>) -> TileType {
        TileType {
            name: name.to_owned(),
            map_symbol: map_symbol.to_owned(),
            is_blocking,
            resources_per_harvest: ResourceStorage::new(resources_per_harvest),
        }
    }
}