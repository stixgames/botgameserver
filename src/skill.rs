pub use cgmath::*;

use command::*;
use entity::*;
use math::*;
use errors::*;
use item::*;
use resource::*;
use map::*;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Skill {
    pub name: String,
    pub effect: Effect,
    pub uses: Option<u16>,
    pub costs: ResourceStorage,
    pub chargeup: u8,
    pub cooldown: u8,
    cooldown_left: u8,
    //TODO: Add costs. Those should be skillpoints or any other resource
}

impl Skill {
    pub fn is_on_cooldown(&self) -> bool {
        self.cooldown_left > 0
    }

    pub fn decrease_cooldown(&mut self) {
        if self.cooldown_left > 0 {
            self.cooldown_left -= 1;
        }
    }

    pub fn put_on_cooldown(&mut self) -> Result<()> {
        if self.cooldown_left > 0 {
            bail!("Skill is still on cooldown.");
        }

        self.cooldown_left = self.cooldown;
        Ok(())
    }

    pub fn decrease_uses(&mut self) {
        if let Some(ref mut uses) = self.uses {
            *uses -= 1;
        }
    }

    /// Construct a skill that can be used continuously, without cooldown or chargeup
    pub fn new_continuous(name: &str, effect: Effect) -> Skill {
        Skill {
            name: name.to_owned(),
            effect: effect,
            uses: None,
            costs: ResourceStorage::empty(),
            chargeup: 0,
            cooldown: 0,
            cooldown_left: 0,
        }
    }

    pub fn new(name: &str, effect: Effect, uses: Option<u16>, costs: Vec<(Resource, u64)>, chargeup: u8, cooldown: u8) -> Skill {
        Skill {
            name: name.to_owned(),
            effect,
            uses,
            costs: ResourceStorage::new(costs),
            chargeup,
            cooldown,
            cooldown_left: 0,
        }
    }

    pub fn idle() -> Skill {
        Skill::new_continuous("Idle", Effect::Idle)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
#[serde(tag = "type", content = "data")]
pub enum Effect {
    Idle,
    Movement { distance: u16 },
    Teleport { distance: u16 },
    LineAttack { distance: u16, damage: u16 },
    LineBlock { distance: u16 },
    AoE { radius: u16, distance: u16, damage: u16 },
    Heal { amount: u16 },
    Build { items: Vec<Item>},
    Learn { skills: Vec<Skill> },
    Collect,
    Harvest { can_harvest: Vec<Resource> },
    Drop { item_index: usize },
    Destroy { destroy_items: bool, destroy_resources: bool, replace_tile: Option<Tile> }
}

impl Effect {
    ///Returns the intended movement. The first element is the first movement position, not the current
    pub fn get_movement(&self, action: &Action) -> Result<Vec<Vector2<i16>>> {
        match *self {
            Effect::Movement { distance } => {
                action.direction.chain_err(|| "No direction in action")
                    .and_then(|direction| {
                        let dir = direction.get_direction();
                        let mut movement = vec![];
                        for mult in 1..(distance as i16 + 1) {
                            movement.push(dir * mult);
                        }
                        Ok(movement)
                    })
            }
            Effect::Teleport { distance } => {
                action.target
                    .chain_err(|| "No target in action")
                    .and_then(|target| {
                        if magnitude(target) > distance as f32 {
                            bail!("target out of range".to_owned());
                        }
                        Ok(vec![target])
                    })
            }
            _ => { Ok(vec![]) }
        }
    }

    pub fn get_block(&self, action: &Action) -> Result<Vec<Vector2<i16>>> {
        match *self {
            Effect::LineBlock { distance } => {
                action.direction
                    .chain_err(|| "No direction in action")
                    .and_then(|direction| {
                        let dir = direction.get_direction();
                        let mut movement = vec![];
                        for mult in 1..(distance as i16 + 1) {
                            movement.push(dir * mult);
                        }
                        Ok(movement)
                    })
            }
            _ => { Ok(vec![]) }
        }
    }

    pub fn get_attack(&self, action: &Action) -> Result<Vec<(Vector2<i16>, u16)>> {
        match *self {
            Effect::LineAttack { distance, damage } => {
                action.direction
                    .chain_err(|| "No direction in action")
                    .and_then(|direction| {
                        let mut attacks = vec![];
                        for mult in 1..distance + 1 {
                            attacks.push((direction.get_direction() * (mult as i16), damage));
                        }
                        Ok(attacks)
                    })
            }
            Effect::AoE { radius: radius_u16, distance, damage } => {
                action.target
                    .chain_err(|| "No target in action")
                    .and_then(|target| {
                        if magnitude(target) <= distance as f32 {
                            let mut attacks = vec![];
                            let radius = radius_u16 as i16;
                            //Iterate over all fields around target
                            for x in (target.x - radius)..(target.x + radius + 1) {
                                for y in (target.y - radius)..(target.y + radius + 1) {
                                    //If a field is in range, apply damage
                                    if magnitude(vec2(x, y)) <= distance as f32 {
                                        attacks.push((vec2(x, y), damage));
                                    }
                                }
                            }
                            Ok(attacks)
                        } else {
                            bail!("target out of range")
                        }
                    })
            }
            _ => Ok(vec![])
        }
    }

    pub fn apply_other(&self, entity: &mut Entity, map: &mut Map, action: &Action) -> Result<()> {
        match *self {
            Effect::Heal { amount } => {
                entity.heal(amount);
            },
            Effect::Build { ref items } => {
                let mut drop_list = vec![];
                for item in items {
                    drop_list.extend(entity.add_item(item.clone()));
                }
                map.drop_items(entity.pos, drop_list);
            },
            Effect::Learn { ref skills } => {
                for skill in skills {
                    entity.skills.push(skill.clone());
                }
            },
            Effect::Collect => {
                entity.resources.combine(map.collect_resources(entity.pos));

                let items = map.collect_items(entity.pos);
                let mut drop_list = vec![];
                for item in items {
                    drop_list.extend(entity.add_item(item.clone()));
                }
                map.drop_items(entity.pos, drop_list);
            }
            Effect::Harvest { ref can_harvest } => {
                let pos = if let Some(direction) = action.direction {
                    offset_pos(entity.pos, direction.get_direction())
                } else {
                    entity.pos
                };

                let tile = map.get_tile_type(pos);
                for resource in can_harvest {
                    let harvest = tile.resources_per_harvest.get(resource);
                    entity.resources.add(*resource, harvest);
                }
            }
            Effect::Drop { item_index } => {
                unimplemented!();
            }
            Effect::Destroy { destroy_items, destroy_resources, replace_tile } => {
                unimplemented!();
            }
            _ => {}
        }

        Ok(())
    }
}