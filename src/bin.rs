#[macro_use]
extern crate error_chain;

extern crate bot_game_lib;

use std::fs::File;
use std::io::Read;

use serde_json;

use bot_game_lib::*;
use map::*;
use item::*;
use skill::*;
use server::*;
use command::*;
use errors::*;
use resource::*;

quick_main!(run);

fn run() -> Result<()> {
    println!("Possible Requests: ");
    println!("{}", serde_json::to_string(&Request::Connection { name: "Player".to_owned() })?);
    println!("{}", serde_json::to_string(&Request::RconConnect { password: "Player".to_owned() })?);
    println!("{}", serde_json::to_string(&Request::ExecuteCommand(Command::Idle))?);
    println!("{}", serde_json::to_string(&Request::ExecuteCommand(Command::Skill { skill_index: 0, direction: Some(Direction::Up), target: None }))?);
    println!("{}", serde_json::to_string(&Request::ExecuteCommand(Command::Skill { skill_index: 0, direction: None, target: Some(vec2(-1, 5)) }))?);
    println!("{}", serde_json::to_string(&Request::RconRestart)?);
    println!();

    println!("Possible Responses: ");
    println!("{}", serde_json::to_string(&Response::RconAccepted)?);
    println!("{}", serde_json::to_string(&Response::PlayerAccepted { key: "XXXXXXXXXXX".to_owned() })?);
    println!("{}", serde_json::to_string(&Response::Error("Error message".to_owned()))?);
    println!("{}", serde_json::to_string(&Response::ConnectionRefused("Error message".to_owned()))?);

    println!();
    println!();

    let map = Map::create_map();

    let items = vec![
        Item::new_simple_weapon("Rusty sword", 10),
        Item::new_simple_armor("Dirty clothes", 0),
    ];

    println!("{}", serde_json::to_string(&items)?);

    let skills = vec![
        Skill::new_continuous("Walk", Effect::Movement { distance: 1 }),
        Skill::new("Teleport", Effect::Teleport { distance: 5 }, None, vec![], 1, 5),
        Skill::new("Collect", Effect::Collect, None, vec![], 0, 0),
        Skill::new_continuous("Harvest wood", Effect::Harvest { can_harvest: vec![Resource::Wood] }),
        Skill::new("Build Pickaxe", Effect::Build {
            items: vec![
                Item::new_consumable("Pickaxe", Skill::new("Harvest stone", Effect::Harvest { can_harvest: vec![Resource::Stone] }, Some(10), vec![], 0, 0))
            ]
        }, None, vec![(Resource::Wood, 5)], 1, 0),
        Skill::new("Learn meteor strike", Effect::Learn {
            skills: vec![
                Skill::new("Meteor strike", Effect::AoE { distance: 10, radius: 3, damage: 100 }, None, vec![], 5, 15)
            ]
        }, Some(1), vec![(Resource::SkillPoint, 5)], 0, 0),
    ];

    println!("{}", serde_json::to_string(&skills)?);

    let pw = File::open("adminpw.txt").and_then(|mut file| {
        let mut pw = String::new();
        file.read_to_string(&mut pw)?;
        Ok(pw)
    }).unwrap_or("password".to_owned());

    let mut server = Server::new(map, pw, items, skills);
    server.start("127.0.0.1:8075").chain_err(|| "Server was stopped because of an error.")?;

    println!("Server was stopped.");

    Ok(())
}
