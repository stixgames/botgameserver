use skill::Skill;

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Item {
    pub name: String,
    pub item_data: ItemData,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
#[serde(tag = "type", content = "data")]
pub enum ItemData {
    Weapon { attack: u16 },
    Armor { defense: u16 },
    Consumable { skill: Skill },
}

impl Item {
    pub fn new_simple_weapon(name: &str, attack: u16) -> Item {
        Item {
            name: name.to_string(),
            item_data: ItemData::Weapon {
                attack: attack
            },
        }
    }

    pub fn new_simple_armor(name: &str, defense: u16) -> Item {
        Item {
            name: name.to_string(),
            item_data: ItemData::Armor {
                defense: defense
            },
        }
    }

    pub fn new_consumable(name: &str, skill: Skill) -> Item {
        assert!(skill.uses.is_some(), "Consumables require a max usage, or they permanently disable a item slot. \
            If you want to create a item that permanently adds a skill (like a spell tome) you can give it a learn Skill.");

        Item {
            name: name.to_string(),
            item_data: ItemData::Consumable {
                skill: skill,
            },
        }
    }

    pub fn is_weapon(&self) -> bool {
        match self.item_data {
            ItemData::Weapon { attack: _ } => true,
            _ => false
        }
    }

    pub fn is_armor(&self) -> bool {
        match self.item_data {
            ItemData::Armor { defense: _ } => true,
            _ => false
        }
    }

    pub fn is_consumable(&self) -> bool {
        match self.item_data {
            ItemData::Consumable { skill: _ } => true,
            _ => false
        }
    }

    pub fn get_item_skill(&self) -> Option<&Skill> {
        match self.get_item_data() {
            &ItemData::Weapon { attack: _ } => None,
            &ItemData::Armor { defense: _ } => None,
            &ItemData::Consumable { ref skill } => Some(skill),
        }
    }

    pub fn get_item_skill_mut(&mut self) -> Option<&mut Skill> {
        match self.get_item_data_mut() {
            &mut ItemData::Weapon { attack: _ } => None,
            &mut ItemData::Armor { defense: _ } => None,
            &mut ItemData::Consumable { ref mut skill } => Some(skill),
        }
    }

    pub fn get_item_data(&self) -> &ItemData {
        &self.item_data
    }

    fn get_item_data_mut(&mut self) -> &mut ItemData {
        &mut self.item_data
    }
}